package au.edu.newcastle.threads.volatilememory;

import java.io.PrintWriter;

/**
 * This class is the volatile example for the program. The only difference
 * between this class and the other class is that this class has its changeMe
 * variable declared as a regular variable. This means that when a thread
 * accesses the variable, it can choose to use a cached value of it to improve
 * performance. However in a multi-threaded environment this can cause issues as
 * changes may not be seen.
 *
 * @author Trent Houliston
 */
public class NonVolatileExample
{

	/**
	 * Our variable of interest, note that it is as a regular variable. This
	 * means that the thread is allowed to cache the value of this variable for
	 * future use.
	 */
	private static int changeMe = 0;
	/**
	 * This is the output to the listener side of the screen
	 */
	private static PrintWriter listenOut;
	/**
	 * This is the output to the changer side of the screen
	 */
	private static PrintWriter changeOut;

	/**
	 * This method runs the actual simulation. It will start up both threads
	 * which will update the variable and output what they see. The changer will
	 * output when it changes the variable, and the listener will output when it
	 * sees the variable change. If a change isn't seen within 3 seconds then
	 * the listener will be killed so the program doesn't hang.
	 *
	 * @param listenOut this is the output for the listener
	 * @param changeOut this is the output for the changer
	 */
	public static void run(PrintWriter listenOut, PrintWriter changeOut)
	{
		//Store our outputs
		NonVolatileExample.listenOut = listenOut;
		NonVolatileExample.changeOut = changeOut;

		//Reset our variable
		NonVolatileExample.changeMe = 0;

		//Make two new threads
		Listener listener = new Listener();
		Changer changer = new Changer();

		//Run the threads
		listener.start();
		changer.start();

		//Wait for 3 seconds so that if there is no change after this time we
		//will kill the simulation
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException ex)
		{
		}

		//If we havent finished after 3 seconds then interrupt the thread (otherwise it runs forever)
		listener.interrupt();

	}

	/**
	 * This class is the Listener. It keeps looping around checking if a remote
	 * variable is equal to its local variable and updating itself. When it sees
	 * a change to this variable it will output that it saw the change.
	 */
	private static class Listener extends Thread
	{

		/**
		 * This is the run method of the simulation
		 */
		@Override
		public void run()
		{
			//Store the varible in a local field
			int local_value = changeMe;
			while (local_value < 5)
			{
				//If the variable has changed
				if (local_value != changeMe)
				{
					//Output that we saw it change
					listenOut.println("Saw change to " + changeMe);
					local_value = changeMe;
				}
				//If we are interrputed that means that the thread was told to cancel
				else if (Thread.currentThread().isInterrupted())
				{
					//Output that we were canceled
					listenOut.println("I didn't see anything?");
					break;
				}
			}
		}
	}

	/**
	 * This thread is the changer class it waits for 0.5 seconds at a time and
	 * increments the variable until it is 5, then it ends
	 */
	private static class Changer extends Thread
	{

		/**
		 * This is the entry point for the thread, it runs the simulation
		 */
		@Override
		public void run()
		{

			//Get the local variable
			int local_value = changeMe;

			//While it's less then 5
			while (changeMe < 5)
			{
				//Output that we are changing the variable
				changeOut.println("Changed variable to " + (local_value + 1));

				//Change the variable
				local_value++;
				changeMe = local_value;
				try
				{
					//Wait 0.5 seconds
					Thread.sleep(500);
				}
				catch (InterruptedException ex)
				{
				}
			}
		}
	}
}