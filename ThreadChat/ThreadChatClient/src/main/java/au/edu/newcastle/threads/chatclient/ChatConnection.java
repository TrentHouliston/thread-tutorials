package au.edu.newcastle.threads.chatclient;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author trent
 */
public class ChatConnection implements Runnable
{

	/**
	 * This is the default port of the chat protocol (chosen at random) this is
	 * the port we will try to connect to if none is given
	 */
	public static final int DEFAULT_PORT = 10801;
	/**
	 * This is the socket connection to the server
	 */
	private final Socket server;
	/**
	 * This is the scanner we will use to read data that comes in from the
	 * socket
	 */
	private final Scanner in;
	/**
	 * This is what we will use to send data back to the server
	 */
	private final PrintWriter out;
	/**
	 * This is simply used so that only a single is typing message is sent
	 * (rather then for every click)
	 */
	private boolean typing = false;

	/**
	 * This creates a new chat connection to the passed address. This address is
	 * parsed and we will try to get a hostname and a port from it. If we can't
	 * understand it we will just throw an exception and then the system will
	 * quit.
	 *
	 * @param address a string representing the host and the port which we are
	 *                   connecting to
	 *
	 * @throws IOException
	 */
	public ChatConnection(String address) throws IOException
	{
		InetSocketAddress addr = null;
		//Need to parse the string that they give us into a useable string
		//We're going to assume they either gave us nothing (connect to localhost)
		if (address.isEmpty())
		{
			addr = new InetSocketAddress("127.0.0.1", DEFAULT_PORT);
		}
		//Or they gave us just a host (no colon in it or no space)
		else if (address.matches("^[^: ]+$"))
		{
			addr = new InetSocketAddress(address, DEFAULT_PORT);
		}
		//Or they gave us an IP and port (host and then an port)
		else if (address.matches("^[^: ]+[: ]\\d+$"))
		{
			String[] split = address.split("[: ]");
			addr = new InetSocketAddress(split[0], Integer.parseInt(split[1]));
		}
		//If nothing matched then just throw an exception
		else
		{
			throw new IOException("Invalid address");
		}

		//Create a new socket object and then connect it to our remote server
		server = new Socket();
		server.connect(addr, 1000);

		//Get the input and output streams
		in = new Scanner(server.getInputStream());
		out = new PrintWriter(server.getOutputStream(), true);
	}

	/**
	 * This is the main loop that the thread which is listening to data from the
	 * server from does. It keeps waiting on the responses so that the UI is
	 * free to continue responding to the user. Otherwise the UI would be locked
	 * up as the thread which is doing the listening to the socket could not
	 * update the UI.
	 */
	@Override
	public void run()
	{
		//Keep going so long as we have data
		String line;
		while (in.hasNextLine())
		{
			//Get our next line
			line = in.nextLine();

			//If it's a message then that means a message came from someone
			if (line.startsWith("message"))
			{
				//Strip off the message part (the first 8 characters) and print the rest
				System.out.println(line.substring(8));
			}
			//If it's a connection message then that means someone connected
			else if (line.startsWith("connect"))
			{
				//Add them to the clients table
				ChatClientsTable.getInstance().setClient(line.substring(8), false);
			}
			//If it's someone disconnecting then we want to remove them
			else if (line.startsWith("disconnect"))
			{
				//Remove them from the table in the UI
				ChatClientsTable.getInstance().removeClient(line.substring(11));
			}
			//If it's someone typing
			else if (line.startsWith("typing"))
			{
				//Update the table and set the user as typing
				ChatClientsTable.getInstance().setClient(line.substring(7), true);
			}
			//If someone stopped typing
			else if (line.startsWith("nottyping"))
			{
				//Update the UI set them to be not typing
				ChatClientsTable.getInstance().setClient(line.substring(10), false);
			}
		}
	}

	/**
	 * This method is used to send a message through the socket to the server.
	 * It will wrap the message in the API
	 *
	 * @param text the message that we are sending
	 */
	public void sendMessage(String text)
	{
		//Output a string that says message and then the text
		out.println(String.format("message %s", text));

		//We are no longer typing
		typing = false;
	}

	/**
	 * This method is used when someone starts or stops typing. It informs the
	 * server of these events, which in turn informs the rest of the clients
	 * (including ourselves)
	 *
	 * @param typing if we are typing
	 */
	public void typing(boolean typing)
	{
		//If there is a change (otherwise don't send anything)
		if (this.typing != typing)
		{
			//Then change it
			this.typing = typing;

			//If we are typing
			if (typing)
			{
				//Send that we are typing
				out.println("typing");
			}
			else
			{
				//Otherwise send that we stopped typing
				out.println("nottyping");
			}
		}
	}
}
