package au.edu.newcastle.threads.chatclient;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;

/**
 * This is a helper class that was written so that the content sent to
 * System.out would instead of appearing in the console (which we may not be
 * able to see) it will instead be fed into a Text Area on the GUI.
 *
 * @author Trent Houliston
 */
public class JTextAreaOutputStream extends OutputStream
{

	/**
	 * The amount of space which will be stored before a flush is forced
	 */
	private static int BUFFER_SIZE = 8192;
	/**
	 * The target text area we are outputting to
	 */
	private JTextArea target;
	/**
	 * The byte buffer we are using
	 */
	private byte[] buffer = new byte[BUFFER_SIZE];
	/**
	 * Our position in the buffer
	 */
	private int pos = 0;

	/**
	 * This constructs a new TextAreaOutputStream wrapped around the passed text
	 * area
	 *
	 * @param target the target text area to write to
	 */
	public JTextAreaOutputStream(JTextArea target)
	{
		this.target = target;
	}

	/**
	 * This method is called whenever a byte of data is sent to the stream. In
	 * this method it will buffer content (up until a point) and then flush it
	 * from the buffer sending it to the output.
	 *
	 * @param b the byte to store
	 *
	 * @throws IOException
	 */
	@Override
	public void write(int b) throws IOException
	{
		//Store the byte in the next position
		buffer[pos++] = (byte) b;

		//Append to the TextArea when the buffer is full
		if (pos == BUFFER_SIZE)
		{
			flush();
		}
	}

	/**
	 * This method flushes the content of the buffer to the text area.
	 *
	 * @throws IOException
	 */
	@Override
	public void flush() throws IOException
	{
		//Create a new buffer for the output
		byte[] flush = null;

		//If we haven't used up our buffer then only copy what we have used
		if (pos != BUFFER_SIZE)
		{
			//Make a new array and copy it
			flush = new byte[pos];
			System.arraycopy(buffer, 0, flush, 0, pos);
		}
		else
		{
			//Flush the buffer
			flush = buffer;
		}

		//Output based on this array
		target.append(new String(flush));
		target.setCaretPosition(target.getDocument().getLength());
		pos = 0;
	}
}