package au.edu.newcastle.threads.chatserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class is the Socket server for this example. It starts up a as a new
 * thread and waits for connections from clients. When it gets one it hands off
 * the client to a new runnable and then puts this into a thread pool.
 *
 * This thread pool will then handle all the communication with the client
 * leaving this thread free to continue waiting for new connections. If we did
 * not have multiple threads then we could not wait on the server as we would
 * have to service the client socket. (and then it would be a very lonely chat
 * server)
 *
 * @author Trent Houliston
 */
public class ChatServer extends Thread
{

	/**
	 * This is the port that the server will run on. (this can be made
	 * configurable)
	 */
	private int port = 10801;
	/**
	 * This is the Thread Pool which we use for the clients.
	 */
	private ExecutorService pool = Executors.newCachedThreadPool();

	/**
	 * This is the run method. It will start listening on a port for connections
	 * from clients and then when one comes in it will pass it off to the thread
	 * pool to manage.
	 */
	@Override
	public void run()
	{
		try
		{
			//Output that we are starting up the server
			System.out.println("Setting up the server on port " + port);

			//Start up the server
			ServerSocket serv = new ServerSocket(port);

			//Loop forever, we will keep serving connections until the application closes
			while (true)
			{
				//This will block and wait until a new socket connection is made
				//to the server, it will then return this socket object
				Socket client = serv.accept();

				//Create a new ChatConnection to handle this socket
				ChatConnection con = new ChatConnection(client);

				//Submit this to the thread pool so it can be started
				pool.submit(con);
			}
		}
		catch (IOException ex)
		{
			//If there was an error then output it to the console
			System.out.println("There was an error while setting up the connection");
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * This method returns the singleton instance of the ChatServer
	 *
	 * @return
	 */
	public static ChatServer getInstance()
	{
		//Return the singleton instance
		return Singleton.INSTANCE;
	}

	/**
	 * The constructor for the server is private as it should not be
	 * instantiated (it's a singleton)
	 */
	private ChatServer()
	{
	}

	/**
	 * The clone method throws a clone not supported exception (it's a
	 * singleton)
	 *
	 * @return does not return
	 *
	 * @throws CloneNotSupportedException always as you cannot clone a Singleton
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * This class is the singleton holder for this class. This method of
	 * singleton creation should always be used as it is thread safe unlike the
	 * double lock method, or inefficent like the single lock method.
	 */
	private static final class Singleton
	{

		/**
		 * The singleton instance of the ChatServer
		 */
		private static final ChatServer INSTANCE = new ChatServer();
	}
}
