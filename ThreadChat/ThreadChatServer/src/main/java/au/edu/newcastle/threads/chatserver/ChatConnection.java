package au.edu.newcastle.threads.chatserver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * This class implements the actual chat connection. It handles parsing all the
 * messages from the clients and sending them out to the other servers.
 *
 * @author Trent Houliston
 */
public class ChatConnection implements Runnable
{

	/**
	 * This is a static set of all the connections that are connected to the
	 * server.
	 */
	private static Set<ChatConnection> clients = Collections.synchronizedSet(new HashSet<ChatConnection>());
	/**
	 * This is the socket which is connected to the chat client
	 */
	private final Socket client;
	/**
	 * This is our scanner which is used to get line by line input
	 */
	private final Scanner in;
	/**
	 * This is our PrintWriter which we
	 */
	private final PrintWriter out;

	/**
	 * This creates a new ChatConnection which handles
	 *
	 * @param client the socket that the client is connecting on
	 *
	 * @throws IOException if there was an exception while getting the streams
	 *                        from the client
	 */
	public ChatConnection(Socket client) throws IOException
	{
		//Store our variables
		this.client = client;
		this.in = new Scanner(client.getInputStream());
		this.out = new PrintWriter(client.getOutputStream(), true);
	}

	/**
	 * This is the main runnable method for this thread. It listens for events
	 * on this socket and sends them to the other sockets.
	 */
	@Override
	public void run()
	{
		//Output that we have a new connection
		System.out.println("New connection from " + this);

		//Add this client to the clients list
		clients.add(this);

		//Tell the chat server that we have a new client
		ChatClientsTable.getInstance().setClient(this, false);

		//Send to us all the clients that connected! (except ourselves we will do that in a second)
		for (ChatConnection c : clients)
		{
			if (c != this)
			{
				//Output all the clients that connected
				out.println("connect " + c);
			}
		}

		//Output to everyone (including ourselves) that we connected)
		broadcast("connect " + this);

		//While we have lines (when this is false the client has disconnected)
		while (in.hasNextLine())
		{
			//Get the line
			String line = in.nextLine();

			//If they are typing
			if (line.equals("typing"))
			{
				//Output to the console that they are typing
				System.out.printf("Client %s is typing\n", this);

				//Tell the chat server we are typing (for the gui)
				ChatClientsTable.getInstance().setClient(this, true);

				//Output to everyone that we are typing
				broadcast("typing " + this.toString());
			}
			//If we are not typing (we deleted everyting)
			else if (line.equals("nottyping"))
			{
				//Output to the console that they are not typing
				System.out.printf("Client %s stopped typing\n", this);

				//Tell everyone we stopped typing
				broadcast("nottyping " + this.toString());

				//Tell the chat server that we are no longer typing (for the gui)
				ChatClientsTable.getInstance().setClient(this, false);
			}
			//If we sent a message
			else if (line.startsWith("message"))
			{
				//Get the message bit of the message
				String message = line.substring(8);

				//Output this message to the console
				System.out.printf("Client %s sent message {\"%s\"}\n", client.getInetAddress().getHostAddress(), message);

				//Tell the chat server that we are now idle (we sent a message!) for the gui
				ChatClientsTable.getInstance().setClient(this, false);

				//Tell everyone the message
				broadcast("message " + this + " " + message);

				//Tell everyone we are no longer typing
				broadcast("nottyping " + this);
			}
		}

		/*
		 * When we are done try to close all the streams this step is important
		 * as if we do not close them, they keep using up resources in the java
		 * virutal machine and we can eventually crash.
		 */
		try
		{
			client.getInputStream().close();
			client.getOutputStream().close();
			client.close();
		}
		catch (IOException ex)
		{
		}

		//Broadcast to all the clients that we just disconnected
		broadcast("disconnect " + this.toString());

		//Output to the console that the connection closed
		System.out.println("Connection from " + client.getInetAddress().getHostAddress() + " has closed");

		//Remove this client from the GUI
		ChatClientsTable.getInstance().removeClient(this);

		//Remove this client from the list of clients to send to
		clients.remove(this);
	}

	/**
	 * This method is used to write a line of data to this client. When the line
	 * is sent via this method, it will be outputted to the client at the other
	 * end of the socket
	 *
	 * @param line
	 */
	private void write(String line)
	{
		//Output the line
		out.println(line);
	}

	/**
	 * This method is used to broadcast a message to every chat client
	 * (including us)
	 *
	 * @param string
	 */
	private void broadcast(String string)
	{
		//Loop through all the connections, (remember our connection is in here too)
		for (ChatConnection c : clients)
		{
			//Write the string
			c.write(string);
		}
	}

	/**
	 * This method is used to return the identity of this client. It is sent as
	 * the IP address followed by a colon then the port.
	 *
	 * @return
	 */
	@Override
	public String toString()
	{
		//The client then colon then the port
		return String.format("%s:%d", client.getInetAddress().getHostAddress(), client.getPort());
	}
}
