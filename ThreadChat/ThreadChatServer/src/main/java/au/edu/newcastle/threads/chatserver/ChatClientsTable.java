package au.edu.newcastle.threads.chatserver;

import javax.swing.table.DefaultTableModel;

/**
 * This class is responsible for maintaining the clients list in the GUI. It
 * receives commands which tell it the state of each client and then it will
 * update the table to show the results of these commands.
 *
 * It is a singleton object so only one instance of it will ever exist. It works
 * by scanning through the existing table for an object when a set operation is
 * done on it. If it finds it then it will set the state otherwise it will
 * remove it from the table. If it is told to remove then it will try to find
 * and remove the listed object.
 *
 * @author Trent Houliston
 */
public class ChatClientsTable
{

	/**
	 * This is our table model which we will use
	 */
	private DefaultTableModel model;

	/**
	 * Gets the singleton instance (the only instance) of this object
	 *
	 * @return the singleton instance of this object
	 */
	public static ChatClientsTable getInstance()
	{
		//Get the instance from the holder class
		return Singleton.INSTANCE;
	}

	/**
	 * This method is used to set the table model (the table model is the part
	 * of the table which holds all the data) into the singleton so it can be
	 * edited.
	 *
	 * @param model the table model to set
	 */
	public void setTableModel(DefaultTableModel model)
	{
		//Store the table model
		this.model = model;
	}

	/**
	 * This method is used to remove a client from the table model. It will scan
	 * through the table looking for this particular entry and if it finds it
	 * then it will remove it from the table model.
	 *
	 * @param client the client which we are removing from the table model
	 */
	public void removeClient(Object client)
	{
		/**
		 * We synchronize here on the table model to get it's monitor lock. We
		 * don't want multiple threads trying to add or remove entries from the
		 * table model while we are working on it or it will confuse our
		 * program.
		 */
		synchronized (model)
		{
			//Loop through all the rows
			for (int i = 0; i < model.getRowCount(); i++)
			{
				//Get the object that's at this position
				Object chat = model.getValueAt(i, 0);

				//If it's the one we are looking for
				if (chat.equals(client))
				{
					//Remove it and return from the method
					model.removeRow(i);
					return;
				}
			}
		}

		//If we got here it wasn't in the list... oh well
	}

	/**
	 * This method sets the clients state and if the client does not exist, then
	 * it will add the client to the list with the specified state. This avoids
	 * the need for a separate add method.
	 *
	 * @param client the client which we are adding or updating on the table
	 *                  model
	 * @param state  the state which we are going to set on the table
	 */
	public void setClient(Object client, boolean state)
	{
		/**
		 * We synchronize here on the table model to get it's monitor lock. We
		 * don't want multiple threads trying to add or remove entries from the
		 * table model while we are working on it or it will confuse our
		 * program.
		 */
		synchronized (model)
		{
			//Now we loop through all of the rows in the table model looking for the one that was passed
			for (int i = 0; i < model.getRowCount(); i++)
			{
				//Get the value at this position in the table
				Object chat = model.getValueAt(i, 0);

				//Check if it's the one we are looking for
				if (chat.equals(client))
				{
					//Set the value on it
					model.setValueAt(state, i, 1);
					return;
				}
			}

			//If we couldn't find it then add a new row with the object and state
			model.addRow(new Object[]
					{
						client, state
					});
		}
	}

	/**
	 * Since this is a singleton object, we don't allow cloning (if we did it
	 * wouldn't be a singleton)
	 *
	 * @return does not return throws an exception
	 *
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * This is the holder class for the singleton instance. This is the correct
	 * way to do a singleton object as it is thread safe.
	 */
	private static final class Singleton
	{

		/**
		 * The singleton instance of the ChatClientsTable
		 */
		private static final ChatClientsTable INSTANCE = new ChatClientsTable();
	}
}
