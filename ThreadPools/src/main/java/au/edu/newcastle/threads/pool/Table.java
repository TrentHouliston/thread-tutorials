package au.edu.newcastle.threads.pool;

import javax.swing.table.DefaultTableModel;

/**
 * This class just holds methods to update our table in our GUI. It holds the
 * Table model and we use it to add new tasks that we are submitting to the
 * queue. They will be updated with their status and when their results are
 * collected, they will be inserted into the system.
 *
 * @author Trent Houliston
 */
public class Table
{

	//Our table model which holds the data for our table
	private DefaultTableModel model;

	/**
	 * This method sets the table model that we are using in our GUI into this
	 * object so we can access and use it.
	 *
	 * @param model the model that we are going to use
	 */
	public void setModel(DefaultTableModel model)
	{
		this.model = model;
	}

	/**
	 * THis method adds a new job into the system. It makes a new
	 *
	 * @param jobId
	 */
	public void addJob(int jobId)
	{
		//Add a new row with this job in it
		model.addRow(new Object[]
				{
					jobId, null, null
				});
	}

	/**
	 * Set the job status of a particular row in the table with the passed
	 * status.
	 *
	 * @param jobId  the job id of the row to set
	 * @param status the status to set the row to
	 */
	public void setStatus(int jobId, String status)
	{
		//Set the value of the status at this row to the passed status
		model.setValueAt(status, jobId, 1);
	}

	/**
	 * Set the result of a particular row
	 *
	 * @param jobId  the job id of the row to set
	 * @param result the result to set
	 */
	public void setResult(int jobId, int result)
	{
		//Set the result of the row to the passed result
		model.setValueAt(result, jobId, 2);
	}

	/**
	 * Gets the singleton instance of the Table object
	 *
	 * @return the singleton instance of the table object
	 */
	public static Table getInstance()
	{
		return Singleton.INSTANCE;
	}

	/**
	 * A private constructor so we cannot construct instances of this object
	 */
	private Table()
	{
	}

	/**
	 * You can't clone a singleton object otherwise it wouldn't be a singleton
	 *
	 * @return
	 *
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * This holds the singleton instance of the Table class
	 */
	private static final class Singleton
	{

		//Create the singleton instance
		private static final Table INSTANCE = new Table();
	}
}
