package au.edu.newcastle.threads.pool;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This object is the object which we are submitting to the thread pool as a
 * task. It implements the Callable interface. The callable interface is like
 * the runnable interface in how it behaves (can be submitted to a thread pool),
 * however unlike the runnable interface the callable interface also returns a
 * result. This works well for tasks as you can submit them to a thread pool and
 * come back and get the result of their calculations later.
 *
 * @author Trent Houliston
 */
public class ThreadTask implements Callable<Integer>
{

	//This is our ID we are going to use, we are using an atomic integer so that it is thread safe
	private static AtomicInteger idPool = new AtomicInteger(0);
	//This is our random number generator we will use to get the random integers
	private Random r = new Random();
	//This is the ID of the object (the index of when it was created)
	private final int id;

	/**
	 * This creates a new thread task, with an ID of one greater then the
	 * previous one (starting at 0)
	 */
	public ThreadTask()
	{
		//Get the atomic value and then increment it by one
		//Since this is atomic it all happens in one step
		this.id = idPool.getAndIncrement();
	}

	/**
	 * This is the call method which is perscribed by the callable interface. It
	 * is a method which returns when it is done of a type specified by the
	 * generic type of the interface (in this case Integer)
	 *
	 * @return a random integer in this case, can be whatever is required by the
	 *            interface
	 *
	 * @throws Exception this exception can be thrown since the method may throw
	 *                      an exception. This is often rethrown later when you
	 *                      try to get the result of the run.
	 */
	@Override
	public Integer call() throws Exception
	{
		//Tell the table we are running
		Table.getInstance().setStatus(id, "running");

		//Wait two seconds
		Thread.sleep(2000);

		//Get a random number
		int nextInt = r.nextInt();

		//Tell the table we are done
		Table.getInstance().setStatus(id, "finished");

		//Return the value
		return nextInt;
	}

	/**
	 * This method gets the ID of this object (a number saying which order it
	 * was created)
	 *
	 * @return the integer index of when this was created
	 */
	public int getId()
	{
		return id;
	}
}
