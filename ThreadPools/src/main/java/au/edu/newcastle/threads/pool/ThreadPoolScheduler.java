package au.edu.newcastle.threads.pool;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is the class which handles the Thread Pool object and creating new
 * tasks for it to run. It also stores the future results of the jobs in a queue
 * through the Future interface. The Future object is a special object which is
 * returned when one of the Thread Pools asynchronous jobs starts. This object
 * can be thought of as holding the results before they are done. If you call
 * the get method on the Future object, it will either return the value (if the
 * task is already done) or it will block the program and wait until there is a
 * result to get.
 *
 * @author Trent Houliston
 */
public class ThreadPoolScheduler
{

	//This is the queue where we will store the future results of the processes
	private Queue<Future<Integer>> results = new LinkedList<Future<Integer>>();
	/**
	 * This is the actual thread pool. Java provides several thread pool
	 * options. The one we are using here is the Fixed thread pool, this thread
	 * pool will always have 3 threads (no more no less) available for work. If
	 * one of the threads dies a new one is created to do it's job.
	 *
	 * The other main type of thread pool which can be made is a cached thread
	 * pool. This type of thread pool creates as many threads as it needs to
	 * work on all the tasks that it has, and when it is done it will keep the
	 * threads it made for about 30 seconds and reuse them for any new tasks
	 * which come up.
	 */
	private ExecutorService threadPool = Executors.newFixedThreadPool(3);
	//This is an atomic integer which is used to track what jobs results we need to get next
	private AtomicInteger currentJob = new AtomicInteger(0);

	/**
	 * This method adds a new task to the thread pools queue and adds the future
	 * result into our results queue.
	 */
	public void addTask()
	{
		//Create a new task
		ThreadTask task = new ThreadTask();

		//Tell the table we have a new task (for the GUI)
		Table.getInstance().addJob(task.getId());

		//Submit the task to the thread pool and get our Future result back
		Future<Integer> submit = threadPool.submit(task);

		//Store this in the queue for evaluation later
		results.add(submit);
	}

	/**
	 * This method gets the result of the next task which has completed (or
	 * waits for it to complete) and puts the value into the table.
	 */
	public void getResult()
	{
		//Get the next future result from the queue
		Future<Integer> poll = results.poll();

		//If we didn't have anything on the queue nothing is running (ignore)
		if (poll != null)
		{
			try
			{
				//Set the value of the current job in the table, to the result of
				//the run (wait until it finishes if nessacary)
				Table.getInstance().setResult(currentJob.getAndIncrement(), poll.get());
			}
			catch (InterruptedException ex)
			{
				/**
				 * This exception will never happen in the running of this
				 * example, This exception occurs when a thread has its
				 * Interrupt method called and is blocked waiting on something
				 * (in this case the get() method if the process hasn't
				 * finished). An example of when this would occur is shown
				 * below.
				 */
				Thread t = new Thread();
				t.interrupt();
				/*
				 * After this the thread T has an interrupted excepiton thrown
				 * if it was waiting on something
				 */
			}
			catch (ExecutionException ex)
			{
				/**
				 * This exception won't ever occur in this particular example,
				 * however what this exception is is when the thread pool runs
				 * the Callable object which it is running can throw an
				 * exception during it's execution. Normally the thread that is
				 * getting the results wouldn't know this if it never checked
				 * the future result. However once it does check the future
				 * result the exception that was thrown by the method is re
				 * thrown wrapped in this exception object.
				 */
			}
		}
	}

	/**
	 * This method gets the Singleton instance of the ThreadPoolScheduler
	 * object.
	 *
	 * @return the singleton instance of the ThreadPoolScheduler
	 */
	public static ThreadPoolScheduler getInstance()
	{
		return Singleton.INSTANCE;
	}

	/**
	 * A private constructor so this singleton object cannot be constructed by
	 * other classes.
	 */
	private ThreadPoolScheduler()
	{
	}

	/**
	 * Cloning is not permitted for a singleton
	 *
	 * @return does not return
	 *
	 * @throws CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}

	/**
	 * This is the singleton holder for the ThreadPoolScheduler singleton
	 */
	private static final class Singleton
	{

		//The Singleton Instance
		private static final ThreadPoolScheduler INSTANCE = new ThreadPoolScheduler();
	}
}
