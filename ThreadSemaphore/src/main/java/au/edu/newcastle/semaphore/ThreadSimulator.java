package au.edu.newcastle.semaphore;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is the actual thread implementation. It waits on each click by the
 * GUI to be notified and then will proceed with the next step of its execution.
 * It might require a permit from the semaphore for the next step of it's
 * execution and will then block until it can get a free permit from the
 * semaphore
 *
 * @author Trent Houliston
 */
public class ThreadSimulator implements Runnable
{

	/**
	 * This is java's implementation of a semaphore. It is used to get permits
	 * from and then return them when it is done.
	 */
	private final Semaphore semaphore;
	/**
	 * This is the callback class object which we can call the method on to
	 * update the GUI.
	 */
	private final Callback callback;

	/**
	 * This constructs a new ThreadSimulator runnable object we can use in the
	 * thread to handle a single thread of the Semaphore example.
	 *
	 * @param semaphore the java semaphore object which we are going to use for
	 *                     the execution.
	 * @param callback  the callback object which we will use to update the GUI
	 *                     at each step.
	 */
	public ThreadSimulator(Semaphore semaphore, Callback callback)
	{
		this.semaphore = semaphore;
		this.callback = callback;
	}

	/**
	 * This is the main run method for this object. When the thread is created
	 * and run it will start its execution here. It will wait at each of the
	 * steps for the GUI to let it know it can continue (it does this using
	 * itself as a monitor) and also at points will require a permit from the
	 * semaphore in order to continue its execution.
	 */
	@Override
	public void run()
	{
		try
		{
			/*
			 * Following pattern
			 *
			 * X - no semaphore
			 *
			 * M - semaphore
			 *
			 * M - semaphore
			 *
			 * X - no semaphore
			 *
			 * M - semaphore
			 */

			//Initial waiting step
			callback.call(0, false, false);
			waitForStep();

			callback.call(1, false, false);
			callback.call(2, false, false);

			waitForStep();

			//Waiting for the semaphore step
			callback.call(3, true, false);

			/**
			 * Here we will try to aquire a permit from the semaphore. In this
			 * example the semaphore has two permits which it can distribute. If
			 * there are no permits available then the thread will block here
			 * and wait until a permit is made available by another thread
			 * releasing theirs. If there is a permit available however, this
			 * thread will take it and be able to continue executing.
			 */
			semaphore.acquire();

			/*
			 * If we have reached this point in the execution that means that we
			 * have the a permit from the semaphore
			 */
			callback.call(4, true, true);
			waitForStep();

			//Use again while still holding the semaphore
			callback.call(5, true, true);
			callback.call(6, true, true);

			waitForStep();

			/*
			 * Release the semaphore permit we have back to the semaphore. This
			 * will be immediately taken by any threads waiting for a permit
			 */
			semaphore.release();

			//Do another step without using the semaphore
			callback.call(7, false, false);
			callback.call(8, false, false);
			waitForStep();

			//Use the semaphore again
			callback.call(9, true, false);
			semaphore.acquire();

			callback.call(10, true, true);
			waitForStep();

			//Release the semaphore back again
			semaphore.release();

			//Use again for the last time while still holding the monitor
			callback.call(11, false, false);
			callback.call(12, false, false);
		}
		catch (InterruptedException ex)
		{
			/*
			 * This won't happen in this example it is caused by another thread
			 * calling the Thread.interrupt() method on this threads object.
			 * This will make the thread stop blocking and throw an Interrupted
			 * exception
			 */
		}
	}

	/**
	 * This method waits for the thread to be notified at which point it will
	 * continue and perform the next step.
	 */
	private synchronized void waitForStep()
	{
		try
		{
			//Wait until this object is notified
			this.wait();
		}
		catch (InterruptedException ex)
		{
		}
	}
}
