package au.edu.newcastle.semaphore;

/**
 * This interface is used as the callback. In the ThreadSemaphoreGUI class this
 * interface is implemented by the anonymous classes which update the GUI when
 * the thread runs them.
 *
 * @author Trent Houliston
 */
public interface Callback
{

	/**
	 * This method is called by the ThreadSimulator classes at each significant
	 * step of their execution.
	 *
	 * @param stage          this is a number representing the stage that the
	 *                          program is up to
	 * @param needsSemaphore this is a boolean representing if this thread
	 *                          currently needs a permit from the semaphore to
	 *                          continue its execution
	 * @param hasSemaphore   this is a boolean representing if this thread
	 *                          currently has a permit from the semaphore
	 */
	public void call(int stage, boolean needsMonitor, boolean hasMonitor);
}
