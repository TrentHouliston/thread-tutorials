package au.edu.newcastle.threads.monitor;

/**
 * This interface is used as the callback. In the ThreadMonitorGUI class this
 * interface is implemented by the anonymous classes which update the GUI when
 * the thread runs them.
 *
 * @author Trent Houliston
 */
public interface Callback
{

	/**
	 * This method is called by the ThreadSimulator classes at each significant
	 * step of their execution.
	 *
	 * @param stage        this is a number representing the stage that the
	 *                        program is up to
	 * @param needsMonitor this is a boolean representing if this thread
	 *                        currently needs the monitor to continue its
	 *                        execution
	 * @param hasMonitor   this is a boolean representing if this thread
	 *                        currently has the monitor lock
	 */
	public void call(int stage, boolean needsMonitor, boolean hasMonitor);
}
