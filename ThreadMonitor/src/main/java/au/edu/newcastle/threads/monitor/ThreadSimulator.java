package au.edu.newcastle.threads.monitor;

/**
 * This class is the actual thread implementation. It waits on each click by the
 * GUI to be notified and then will proceed with the next step of its execution.
 * It might require the exclusive lock of the monitor for the next step of it's
 * execution and will then block until it can get control of the monitor.
 *
 * @author Trent Houliston
 */
public class ThreadSimulator implements Runnable
{

	/**
	 * This is a generic java object. However since all java objects have a
	 * built in monitor we can use the monitor for this object as our monitor.
	 */
	private final Object lock;
	/**
	 * This is the callback class object which we can call the method on to
	 * update the GUI.
	 */
	private final Callback callback;

	/**
	 * This constructs a new ThreadSimulator runnable object we can use in the
	 * thread to handle a single thread of the Monitor example.
	 *
	 * @param monitor  the java object which we are going to use as the monitor
	 *                    for the execution.
	 * @param callback the callback object which we will use to update the GUI
	 *                    at each step.
	 */
	public ThreadSimulator(Object monitor, Callback callback)
	{
		this.lock = monitor;
		this.callback = callback;
	}

	/**
	 * This is the main run method for this object. When the thread is created
	 * and run it will start its execution here. It will wait at each of the
	 * steps for the GUI to let it know it can continue (it does this using
	 * itself as a monitor) and also at points will require a exclusive access
	 * to a synchronized block which it will do by obtaining the monitor from
	 * the monitor object.
	 */
	@Override
	public void run()
	{
		/*
		 * Following pattern
		 *
		 * X - no monitor
		 *
		 * M - monitor
		 *
		 * M - monitor
		 *
		 * X - no monitor
		 *
		 * M - monitor
		 */

		//Initial waiting step
		callback.call(0, false, false);
		waitForStep();

		callback.call(1, false, false);
		callback.call(2, false, false);
		waitForStep();

		//Waiting for the monitor step
		callback.call(3, true, false);

		/*
		 * Synchronized blocks in java is how monitors are used. Only a single
		 * thread at a time can be executing within a synchronized block.
		 *
		 * Synchronized blocks are always performed an object which is the
		 * parameter for the block. In this case we are obtaining the monitor
		 * for the lock object.
		 *
		 * A thread executing through this code will stop execution and go to a
		 * blocked state until it can obtain the monitor for the lock object.
		 */
		synchronized (lock)
		{
			/*
			 * If we have reached this point in the execution that means that we
			 * have the monitor, and will have it until we leave this block or
			 * we use the special method .wait() on the monitor.
			 *
			 * If we call monitor.wait() in this block what that means is that
			 * we have effectivly left this synchronized block (although when we
			 * resume we will pick up where we left off). But we will only
			 * resume when another thread calls monitor.notify() or
			 * monitor.notifyAll().
			 *
			 * The difference between these two is that notify() will only
			 * choose one thread which called monitor.wait() to allow to resume
			 * execution, while notifyAll() will allow all threads which called
			 * wait() to resume execution.
			 */
			callback.call(4, true, true);
			waitForStep();

			//Use again while still holding the monitor
			callback.call(5, true, true);
			callback.call(6, true, true);

			waitForStep();
		}

		//Do another step without using the monitor
		callback.call(7, false, false);
		callback.call(8, false, false);
		waitForStep();

		//Use the monitor again
		callback.call(9, true, false);
		synchronized (lock)
		{
			callback.call(10, true, true);
			waitForStep();
		}


		//Use again for the last time while still holding the monitor
		callback.call(11, false, false);
		callback.call(12, false, false);
	}

	/**
	 * This method waits for the thread to be notified at which point it will
	 * continue and perform the next step.
	 */
	private synchronized void waitForStep()
	{
		try
		{
			//Wait until this object is notified
			this.wait();
		}
		catch (InterruptedException ex)
		{
		}
	}
}
