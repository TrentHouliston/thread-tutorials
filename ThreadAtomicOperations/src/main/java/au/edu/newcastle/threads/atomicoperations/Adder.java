package au.edu.newcastle.threads.atomicoperations;

/**
 * This class is the Thread object which does the actual adding. A number of
 * these are created each time the program is run and each of them first wait
 * for a signal (the initial wait section) and then loop through incrementing
 * their variable.
 *
 * @author Trent Houliston
 */
public class Adder implements Runnable
{

	/**
	 * The Add object which we are incrementing using the increment method. This
	 * object is also used to synchronized on so all the threads start at the
	 * same time.
	 */
	private final Add add;
	/**
	 * The variable which stores how many times this thread is going to
	 * increment the add object.
	 */
	private final int amount;

	/**
	 * This constructs a new Adder thread using the passed Add object to be
	 * incremented to, and the amount as the number of times the increment
	 * method will be called.
	 *
	 * @param add    the object which the increment method will be called to
	 *                  add.
	 * @param amount the number of times to call the increment method (the
	 *                  amount to add)
	 */
	public Adder(Add add, int amount)
	{
		//Store the two variables in the final variable locations
		this.amount = amount;
		this.add = add;
	}

	/**
	 * This method is the actual implementation and the starting point of the
	 * thread. All thread objects will begin by running on a run method.
	 */
	@Override
	public void run()
	{
		//This block here is used to make sure that all the threads start at the same time.
		//It obtains the monitor lock on the add variable and then waits on it.
		synchronized (add)
		{
			try
			{
				//Wait on this variable (release the monitor lock and wait to be notified)
				add.wait();
			}
			catch (InterruptedException ex)
			{
				//This will never happen
			}
		}

		//Loop through the number of time we are going to be adding to the variable
		for (int i = 0; i < amount; i++)
		{
			//Call the increment method to add to this variable
			add.increment();
		}
	}
}
