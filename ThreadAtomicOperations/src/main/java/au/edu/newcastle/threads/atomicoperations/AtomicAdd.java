package au.edu.newcastle.threads.atomicoperations;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is the atomic implementation of the Add interface. It's increment
 * uses an atomic integer which means that the entire increment is done in a
 * single operation.
 *
 * @author Trent Houliston
 */
public class AtomicAdd implements Add
{

	/**
	 * The integer which we are going to be incrementing
	 */
	private AtomicInteger i = new AtomicInteger();

	/**
	 * This method is the increment method, It simply adds one to the i
	 * variable. Since it is not synchronized, all the threads can be performing
	 * this concurrently.
	 */
	@Override
	public void increment()
	{
		i.incrementAndGet();
	}

	/**
	 * This method gets the value that this add object currently holds
	 *
	 * @return the current value of this add object
	 */
	@Override
	public int getValue()
	{
		return i.get();
	}
}
