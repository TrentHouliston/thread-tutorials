package au.edu.newcastle.threads.atomicoperations;

/**
 * This interface is used for the two types of adders which are used in this
 * demonstration. There is one implementation of this which uses synchronized
 * methods, and one which uses an atomic integer.
 *
 * @author Trent Houliston
 */
public interface Add
{

	/**
	 * Increments the integer stored in this class
	 */
	public void increment();

	/**
	 * Gets the value of the integer stored in this class
	 *
	 * @return the integer stored in this class
	 */
	public int getValue();
}
