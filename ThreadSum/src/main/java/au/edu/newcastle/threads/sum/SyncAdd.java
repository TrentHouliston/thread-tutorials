package au.edu.newcastle.threads.sum;

/**
 * This class is the synchronized implementation of the Add interface. It's
 * increment method uses the synchronized flag to show that the whole method
 * should be synchronized around the object (the this object).
 *
 * @author Trent Houliston
 */
public class SyncAdd implements Add
{

	/**
	 * The integer which we are going to be incrementing
	 */
	private int i;

	/**
	 * This method is the increment method, It simply adds one to the i
	 * variable. Since it is synchronized only one of the threads can execute
	 * this method at a time.
	 */
	@Override
	public synchronized void increment()
	{
		i++;
	}

	/**
	 * This method gets the value that this add object currently holds
	 *
	 * @return the current value of this add object
	 */
	@Override
	public int getValue()
	{
		return i;
	}
}
