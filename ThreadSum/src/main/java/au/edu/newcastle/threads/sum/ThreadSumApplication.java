package au.edu.newcastle.threads.sum;

import java.util.LinkedList;
import java.util.concurrent.Callable;

/**
 * This is the part of the example that sets up the Thread objects, executes
 * them and returns the result of each of them.
 *
 * @author Trent Houliston
 */
public class ThreadSumApplication implements Callable<Integer[][]>
{

	/**
	 * The number of threads that we are going to make
	 */
	private final int threads;
	/**
	 * The amount we are adding to each of the variables per thread
	 */
	private final int adding;

	/**
	 * Creates a new ThreadSummer object that spawns {threads} threads and each
	 * thread will add {adding} to the total.
	 *
	 * @param threads the number of threads to create
	 * @param adding  the amount ot add for each one
	 */
	public ThreadSumApplication(int threads, int adding)
	{
		this.threads = threads;
		this.adding = adding;
	}

	/**
	 * This method is used to execute the test. It returns a multi dimensional
	 * array containing for both unsynchronized and synchronized (in that order)
	 * the final total amount obtained once all the threads have added their
	 * contribution. And how many milliseconds it took to achieve this.
	 *
	 * @return the multidimensional array containing totals and runtime
	 *
	 * @throws InterruptedException if one of the threads is interrupted while
	 *                                 processing
	 */
	@Override
	public Integer[][] call() throws InterruptedException
	{
		//Create our return array and timer
		Integer[][] results = new Integer[2][2];
		long timer;

		//Time and execute running the unsynchronized execution
		timer = System.currentTimeMillis();
		results[0][0] = run(new UnsyncAdd());
		timer = System.currentTimeMillis() - timer - 10; //-10 since we wait 10 in the method...
		results[0][1] = (int) timer;

		//Time and execute running the synchronized execution
		timer = System.currentTimeMillis();
		results[1][0] = run(new SyncAdd());
		timer = System.currentTimeMillis() - timer - 10; //-10 since we wait 10 in the method...
		results[1][1] = (int) timer;

		//Return the results
		return results;
	}

	/**
	 * Perform the actual run, This is where all the actual thread creation and
	 * getting the results happens.
	 *
	 * @param add the variable which we are going to increment
	 *
	 * @return the final result once we are done incrementing this variable
	 *
	 * @throws InterruptedException if the thread is interrupted while in the
	 *                                 join method.
	 */
	private int run(Add add) throws InterruptedException
	{
		//Create a list of threads that are going to run
		LinkedList<Thread> thread = new LinkedList<Thread>();

		//Populate this list with all the objects
		for (int i = 0; i < threads; i++)
		{
			//Create a new adder object (our thread extention that's going to do the work
			Adder adder = new Adder(add, adding);

			/*
			 * Start the thread. This is a special method of the Thread class
			 * which will when run, start a new running thread which will begin
			 * executing the run() method in the thread. The default thread
			 * object's run() method is empty so it will immediantly terminate,
			 * so to create a thread you must either extend the thread class and
			 * override this run() method (as we have done here). Or you need to
			 * create an object which implements the Runnable interface and pass
			 * this into the Thread object's constructor
			 */
			Thread t = new Thread(adder);
			t.start();

			//Add it to the list
			thread.add(t);
		}

		/*
		 * Here we are sleeping for 10 milliseconds. The reason for this is that
		 * if we dont, then some of the threads may not reach the wait() point
		 * and then will never finish freezing the application.
		 */
		Thread.sleep(10);

		//Notify all the threads that it is time to start. Since we are using
		//the notifyAll method, we must have the lock for that object.
		synchronized (add)
		{
			add.notifyAll();
		}

		//In this step we are waiting for all the threads to finish
		for (Thread t : thread)
		{
			//The join method is a method which results in the current thread
			//waiting until the referenced thread has finished execution
			// (don't call Thread.currentThread().join() :P)
			t.join();
		}

		//Return our final value
		return add.getValue();
	}
}
