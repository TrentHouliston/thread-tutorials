package au.edu.newcastle.threads.sum;

/**
 * This class is the unsynchronized implementation of the Add interface. It's
 * increment method uses does not have the synchronized flag. So all the threads
 * can be in this at once.
 *
 * @author Trent Houliston
 */
public class UnsyncAdd implements Add
{

	/**
	 * The integer which we are going to be incrementing
	 */
	private int i;

	/**
	 * This method is the increment method, It simply adds one to the i
	 * variable. Since it is not synchronized, all the threads can be performing
	 * this concurrently.
	 */
	@Override
	public void increment()
	{
		i++;
	}

	/**
	 * This method gets the value that this add object currently holds
	 *
	 * @return the current value of this add object
	 */
	@Override
	public int getValue()
	{
		return i;
	}
}
