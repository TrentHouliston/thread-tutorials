package au.edu.newcastle.threads.basic;

import java.util.Scanner;

/**
 * This class shows the two basic ways of making a new thread in Java. It also
 * shows a running explanation of the two, and why you should always use the
 * second method.
 *
 * @author Trent Houliston
 */
public class BasicThreads
{

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		try
		{
			//Output the first section of the tutorial
			System.out.println("There are two main ways to create a new thread in java.");
			System.out.println("Firstly you can extend the Thread class and implement its run method");
			System.out.println();
			System.out.println("\tpublic class ExtendThread extends Thread\n"
							   + "\t{\n"
							   + "\t\t@Override\n"
							   + "\t\tpublic void run()\n"
							   + "\t\t{\n"
							   + "\t\t\tSystem.out.println(\"Hi I'm a thread that was made by extending Thread!\");\n"
							   + "\t\t}\n"
							   + "\t}");
			System.out.println();
			System.out.println("And then run it's start method");
			System.out.println();
			System.out.println("\tnew ExtendThread().start()");
			System.out.println();

			System.out.println("(Press enter to continue)");
			in.nextLine();

			System.out.println("and it will do this");

			/**
			 * *****************************************************************
			 * THIS IS THE FIRST IMPORTANT PART OF THIS CLASS, IT STARTS THE
			 * THREAD THAT EXTENDS FROM THE THREAD CLASS
			 * *****************************************************************
			 */
			System.out.println();
			//Create the thread that extends from the Thread class
			Thread run1 = new ExtendThread();
			//Start it
			run1.start();
			//Wait for it to finish
			run1.join();
			System.out.println();

			System.out.println("(Press enter to continue)");
			in.nextLine();

			System.out.println("The second way is to implement the runnable interface and pass this");
			System.out.println("into the constructor of a Thread object");

			System.out.println();
			System.out.println("\tpublic class ViaRunnable implements Runnable\n"
							   + "\t{\n"
							   + "\t\t@Override\n"
							   + "\t\tpublic void run()\n"
							   + "\t\t{\n"
							   + "\t\t\tSystem.out.println(\"Hi I'm a thread that was made by implementing Runnable!\");\n"
							   + "\t\t}\n"
							   + "}");

			System.out.println();
			System.out.println("As follows");
			System.out.println();
			System.out.println("\tnew Thread(new ViaRunnable()).start()");
			System.out.println();

			System.out.println("(Press enter to continue)");
			in.nextLine();

			System.out.println("and it will do this");

			/**
			 * *****************************************************************
			 * THIS IS THE SECOND IMPORTANT PART OF THIS CLASS, IT STARTS THE
			 * THREAD THAT IMPLEMENTS RUNNABLE FROM THE THREAD CLASS
			 * *****************************************************************
			 */
			System.out.println();
			//Create the runnable object
			Runnable runnable = new ViaRunnable();
			//Make a new thread and use this runnable as its run method
			Thread run2 = new Thread(runnable);
			//Start the thread
			run2.start();
			//Wait for it to finish
			run2.join();
			System.out.println();

			System.out.println("(Press enter to continue)");
			in.nextLine();

			//Output the end of the tutorial (explaining why you should use the second way)
			System.out.println("The preferred way to do this is the second way.");
			System.out.println("Both will work almost identically however the second is preferable for three main reasons.");
			System.out.println("Firstly the word extends is used when you are extending the functionality of another class.");
			System.out.println("Most of the time when you are making a thread, you are not extending the functionality of thread,");
			System.out.println("You are simply making something that a thread can run");

			System.out.println("The second reason that implementing runnable is preferable,");
			System.out.println("is that this allows you to extend from another class, and use methods which are already taken by the Thread class");
			System.out.println("(e.g. start() and getState()");

			System.out.println("The third reason is that if you want to use a thread pool (discussed further later), these will use the runnable interface.");
			System.out.println("This is because they are already the executing threads, and rather then needing a new thread, they just need something to run.");
		}
		catch (InterruptedException ex)
		{
			/*
			 * This exception will never happen in this example, below is an
			 * example of how this exception occurs
			 */
			Thread thread = new Thread();
			thread.start();
			thread.interrupt();

			/**
			 * If that thread were waiting for something (waiting for an object,
			 * reading from a socket etc. Then it would throw an interrupted
			 * exception.
			 */
		}
	}
}
