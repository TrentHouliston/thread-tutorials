package au.edu.newcastle.threads.basic;

/**
 * This class implements the runnable interface, This means that it can be
 * passed into the constructor of the Thread object and run. The advantage of
 * this is that you can have this class extend from another class, and have
 * method that are already implemented in the Thread class.
 *
 * @author Trent Houliston
 */
public class ViaRunnable implements Runnable
{

	/**
	 * This is where the new created thread starts running. In this method, it
	 * simply outputs a message and then dies.
	 */
	@Override
	public void run()
	{
		System.out.println("Hi I'm a thread that was made by implementing Runnable!");
	}
}
