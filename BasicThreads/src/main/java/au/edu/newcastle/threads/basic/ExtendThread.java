package au.edu.newcastle.threads.basic;

/**
 * This class extends the Thread object. This means that if an instance of this
 * is created and its start() method is run, then this thread will begin running
 * in it's run() method.
 *
 * An important point to note between this class and the Runnable interface, is
 * that this thread can only be started once, while a runnable can be used
 * across multiple threads multiple times. Sometimes the fact that a thread can
 * only be started once is advantageous and extending this class should be done.
 *
 * @author Trent Houliston
 */
public class ExtendThread extends Thread
{

	/**
	 * This is where the new created thread starts running. In this method, it
	 * simply outputs a message and then dies.
	 */
	@Override
	public void run()
	{
		System.out.println("Hi I'm a thread that was made by extending Thread!");
	}
}
